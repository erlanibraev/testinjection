package kz.madsoft.test;

public class IAImpl implements IA {
    private IB ib;
    IAImpl(IB ib) {
        this.ib = ib;
    }
    public void testA() {
        System.out.println("TEST A");
        ib.testB();
    }
}
