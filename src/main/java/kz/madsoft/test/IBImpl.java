package kz.madsoft.test;

import java.util.Random;

public class IBImpl implements IB {
    private IA ia;
    IBImpl(IA ia) {
        this.ia = ia;
    }
    public void testB() {
        Random random = new Random();
        if(random.nextBoolean()) {
            System.out.println("TEST B");
        } else {
            ia.testA();
        }
    }
}
