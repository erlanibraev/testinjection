package kz.madsoft.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

@SpringBootApplication
public class Main {
    @Bean
    public IA iaBean(@Lazy IB ibBean) {
        return new IAImpl(ibBean);
    }
    @Bean
    public IB ibBean(IA iaBean) {
        return new IBImpl(iaBean);
    }
    @Bean
    public CommandLineRunner commandLineRunner() {
        return new CommandLineRunner() {
            @Autowired
            private IA ia;
            @Autowired
            private IB ib;
            @Override
            public void run(String... args) throws Exception {
                ia.testA();
                System.out.println("--------------");
                ib.testB();
            }
        };
    }
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
